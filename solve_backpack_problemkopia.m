%%%%%%%%%%%%%%%%%%%%%%%%
%       Projekt 2      %
% Algorytmy Ewolucyjne %
%    Marcin Dolicher   %
%%%%%%%%%%%%%%%%%%%%%%%%

function [hstate hoptions hsum] = solve_backpack_problem(items, N)

%skrypt_1;

% historic date from algorithm 
hstate = [];
hoptions = [];
hsum = [];

% first column of variable items is w and second is p 
x=zeros(64,1);
fun = @(x)objectiveFun(x, items(:,2), N) 
A = [items(:,1)'];
b = [sum(items(:,1)).*0.3];
lb = zeros(1,N);
ub = ones(1,N);
IntCon = [1:N];
options = optimoptions('ga','PlotFcn', @gaplotbestf, 'OutputFcn', @historicData, 'Display', 'iter', 'PopulationSize', 50, 'PopulationType', 'bitstring');
[wynik, fval, exitflag, output, population, scores] = ga(fun, N, [], [], [], [], [], [], [], [], options);

%% Funtions implementation 
function [state,options,optchanged] = historicData(options,state,flag)
    optchanged = false;
    switch flag
        case 'iter'
            hstate =  [hstate; state];
            hoptions = [hoptions; options];
    end
end
    
function sum = objectiveFun(x, p, N)
    sum = 0; 
    for i = 1:1:N
       sum = sum + p(i).*x(i);  
    end
    
   if(A*x' > b)
       sum1 = A*x';
       sum2 = b;
       sum=1000;
       return;
   end
    sum=-sum;
end

end