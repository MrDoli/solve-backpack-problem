%%%%%%%%%%%%%%%%%%%%%%%%
%       Projekt 2      %
% Algorytmy Ewolucyjne %
%    Marcin Dolicher   %
%%%%%%%%%%%%%%%%%%%%%%%%

%% Wpisz numer albumu
clear;
numerAlbumu=283402;
rng(numerAlbumu);
N=64;
items(:,1)=round(0.1+0.9*rand(N,1),1);
items(:,2)=round(1+99*rand(N,1));

opis_do_wykresow = '_mutationgaussian_200_45_EliteCount_5';
zapis = 0;

[w_hstate, w_hoptions, hsum] = solve_backpack_problem(items, N);

generation_Number = size(w_hstate);
generation_Number = generation_Number(1,1);

min_ga = [];
ave_ga = [];
max_ga = [];
var_ga = [];

for i=1:1:generation_Number
   w_hstate(i).Score = -w_hstate(i).Score;
   min_ga = [min_ga; min(w_hstate(i).Score)]; 
   ave_ga = [ave_ga; mean(w_hstate(i).Score)];
   max_ga = [max_ga; max(w_hstate(i).Score)];
   var_ga = [var_ga; var(w_hstate(i).Score)];
end

figure(6)
a = plot(min_ga, '*');
hold on 
title('Graph of the minimum values objective function');
hold off

figure(2)
b = plot(ave_ga, '*');
hold on 
title('Graph of the average values objective function');
hold off

figure(3)
c = plot(max_ga, '*');
hold on 
title('Graph of the maximum values objective function');
hold off

figure(4)
hold on
plot(min_ga, 'r*');
plot(ave_ga, 'b*');
d = plot(max_ga, 'g*');
legend('min','ave','max');
title('Graph of the comparing the values of objective function');
hold off

figure(5)
e = plot(var_ga, '*');
hold on 
title('Graph of the variance values objective function');
hold off

if zapis == 1
    % Zapis do pliku
    str = strcat('Wykresy\', 'min_', num2str(N), opis_do_wykresow, '.png');
    saveas(a, str,'png');

    % Zapis do pliku
    str = strcat('Wykresy\', 'ave_', num2str(N), opis_do_wykresow, '.png');
    saveas(b, str,'png');

    % Zapis do pliku
    str = strcat('Wykresy\', 'max_', num2str(N), opis_do_wykresow, '.png');
    saveas(c, str,'png');

    % Zapis do pliku
    str = strcat('Wykresy\', 'all_', num2str(N), opis_do_wykresow, '.png');
    saveas(d, str,'png');

    % Zapis do pliku
    str = strcat('Wykresy\', 'var_', num2str(N), opis_do_wykresow, '.png');
    saveas(e, str,'png');
end