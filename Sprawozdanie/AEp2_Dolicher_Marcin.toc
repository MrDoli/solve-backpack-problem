\babel@toc {polish}{}
\contentsline {part}{\numberline {I\relax .\leavevmode@ifvmode \kern .5em }Projekt}{2}{part.1}% 
\contentsline {chapter}{\numberline {1\relax .\leavevmode@ifvmode \kern .5em }Opis zadania}{3}{chapter.1}% 
\contentsline {part}{\numberline {II\relax .\leavevmode@ifvmode \kern .5em }Liczba przedmiot\'ow r\'owna 32}{4}{part.2}% 
\contentsline {chapter}{\numberline {2\relax .\leavevmode@ifvmode \kern .5em }Uzyskane wyniki podczas rozwi\k azywania problemu plecakowego}{5}{chapter.2}% 
\contentsline {section}{\numberline {2.1\relax .\leavevmode@ifvmode \kern .5em }Zachowanie algorytmu dla domy\'slnych ustawie\'n algorytmu \textit {ga} w programie matlab}{5}{section.2.1}% 
\contentsline {section}{\numberline {2.2\relax .\leavevmode@ifvmode \kern .5em }Wp\IeC {\l }yw zmniejszanie liczby populacji}{9}{section.2.2}% 
\contentsline {section}{\numberline {2.3\relax .\leavevmode@ifvmode \kern .5em }Wp\IeC {\l }yw zwi\k ekszenia liczby populacji}{13}{section.2.3}% 
\contentsline {section}{\numberline {2.4\relax .\leavevmode@ifvmode \kern .5em }Zbadanie wp\IeC {\l }ywu warunku stopu na dzia\IeC {\l }anie algorytmu}{17}{section.2.4}% 
\contentsline {section}{\numberline {2.5\relax .\leavevmode@ifvmode \kern .5em }Badanie wp\IeC {\l }ywu parametr\'ow odpowiadaj\k acych za reprodukcj\k e na dzia\IeC {\l }anie algorytmu}{21}{section.2.5}% 
\contentsline {subsection}{\numberline {2.5.1\relax .\leavevmode@ifvmode \kern .5em }Uzyskane wyniki dla parametru $\textit {elite count} = 2$}{21}{subsection.2.5.1}% 
\contentsline {subsection}{\numberline {2.5.2\relax .\leavevmode@ifvmode \kern .5em }Uzyskane wyniki dla parametru $\textit {elite count} = 10$}{25}{subsection.2.5.2}% 
\contentsline {subsection}{\numberline {2.5.3\relax .\leavevmode@ifvmode \kern .5em }Uzyskane wyniki dla parametru $\textit {elite count} = 15$}{28}{subsection.2.5.3}% 
\contentsline {subsection}{\numberline {2.5.4\relax .\leavevmode@ifvmode \kern .5em }Wnioski po zbadaniu wp\IeC {\l }ywu parametru \textit {elite count}}{31}{subsection.2.5.4}% 
\contentsline {subsection}{\numberline {2.5.5\relax .\leavevmode@ifvmode \kern .5em }Zmiana parametru \textit {crossover fraction} na warto\'s\'c 0.3}{31}{subsection.2.5.5}% 
\contentsline {section}{\numberline {2.6\relax .\leavevmode@ifvmode \kern .5em }Badanie wp\IeC {\l }ywu zmiany algorytm\'ow mutacji na otrzymywane rozwi\k azanie}{34}{section.2.6}% 
\contentsline {subsection}{\numberline {2.6.1\relax .\leavevmode@ifvmode \kern .5em }Zmiana parametr\'ow w funkcji gaussa u\.zywanej podczas mutacji}{34}{subsection.2.6.1}% 
\contentsline {part}{\numberline {III\relax .\leavevmode@ifvmode \kern .5em }Liczba przedmiot\'ow r\'owna 64}{41}{part.3}% 
\contentsline {section}{\numberline {2.7\relax .\leavevmode@ifvmode \kern .5em }Przyj\k ete za\IeC {\l }o\.zenia podczas rozwi\k azania problemu plecakowego dla 64 przedmiot\'ow}{42}{section.2.7}% 
\contentsline {section}{\numberline {2.8\relax .\leavevmode@ifvmode \kern .5em }Wykaz przedmiot\'ow dla kt\'orych szukamy rozwi\k azania problemu plecakowego}{42}{section.2.8}% 
\contentsline {section}{\numberline {2.9\relax .\leavevmode@ifvmode \kern .5em }Zachowanie algorytmu dla domy\'slnych ustawie\'n algorytmu \textit {ga} w programie matlab}{43}{section.2.9}% 
\contentsline {section}{\numberline {2.10\relax .\leavevmode@ifvmode \kern .5em }Wp\IeC {\l }yw zmniejszanie liczby populacji}{46}{section.2.10}% 
\contentsline {section}{\numberline {2.11\relax .\leavevmode@ifvmode \kern .5em }Wp\IeC {\l }yw zwi\k ekszenia liczby populacji}{50}{section.2.11}% 
\contentsline {section}{\numberline {2.12\relax .\leavevmode@ifvmode \kern .5em }Zbadanie wp\IeC {\l }ywu warunku stopu na dzia\IeC {\l }anie algorytmu}{54}{section.2.12}% 
\contentsline {section}{\numberline {2.13\relax .\leavevmode@ifvmode \kern .5em }Badanie wp\IeC {\l }ywu parametr\'ow odpowiadaj\k acych za reprodukcj\k e na dzia\IeC {\l }anie algorytmu}{58}{section.2.13}% 
\contentsline {subsection}{\numberline {2.13.1\relax .\leavevmode@ifvmode \kern .5em }Uzyskane wyniki dla parametru $\textit {elite count} = 2$}{58}{subsection.2.13.1}% 
\contentsline {subsection}{\numberline {2.13.2\relax .\leavevmode@ifvmode \kern .5em }Uzyskane wyniki dla parametru $\textit {elite count} = 10$}{61}{subsection.2.13.2}% 
\contentsline {subsection}{\numberline {2.13.3\relax .\leavevmode@ifvmode \kern .5em }Uzyskane wyniki dla parametru $\textit {elite count} = 15$}{64}{subsection.2.13.3}% 
\contentsline {subsection}{\numberline {2.13.4\relax .\leavevmode@ifvmode \kern .5em }Wnioski po zbadaniu wp\IeC {\l }ywu parametru \textit {elite count}}{67}{subsection.2.13.4}% 
\contentsline {subsection}{\numberline {2.13.5\relax .\leavevmode@ifvmode \kern .5em }Zmiana parametru \textit {crossover fraction} na warto\'s\'c 0.3}{67}{subsection.2.13.5}% 
\contentsline {section}{\numberline {2.14\relax .\leavevmode@ifvmode \kern .5em }Badanie wp\IeC {\l }ywu zmiany algorytm\'ow mutacji na otrzymywane rozwi\k azanie}{70}{section.2.14}% 
\contentsline {subsection}{\numberline {2.14.1\relax .\leavevmode@ifvmode \kern .5em }Zmiana parametr\'ow w funkcji gaussa u\.zywanej podczas mutacji}{70}{subsection.2.14.1}% 
\contentsline {chapter}{\numberline {3\relax .\leavevmode@ifvmode \kern .5em }Podsumowanie}{77}{chapter.3}% 
\contentsline {section}{\numberline {3.1\relax .\leavevmode@ifvmode \kern .5em }Prezentacja wykazu przedmiot\'ow dla poszczeg\'olnych eksperyment\'ow}{77}{section.3.1}% 
